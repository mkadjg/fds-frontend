import React, {Component} from 'react';
import {
    Card,
    CardBody,
    Col,
    Row,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from 'reactstrap';

class CreateUser extends Component{
    constructor(props){
        super(props);
        this.state = {
            username: '',
            name: '',
            role: '',
            password: '',
            confirmPassword: '',
            danger: false,
            role_id: '',
            role_data: []
        }

        this.handleChange = this.handleChange.bind(this);
        this.submitData = this.submitData.bind(this);
        this.toggleDanger = this.toggleDanger.bind(this);
        this.loadRoleData = this.loadRoleData.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount() {
        this.loadRoleData();
    }

    loadRoleData() {
        fetch('http://localhost:5000/roles/', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            this.setState({
                role_data: data
            })
        });
    }

    submitData(event) {
        event.preventDefault();
        if (this.state.password !== this.state.confirmPassword) {
            this.toggleDanger(true);
        } else {
            fetch('http://localhost:5000/user/', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(
                    {
                        username: this.state.username,
                        name: this.state.name,
                        role: this.state.role,
                        role_id: this.state.role_id,
                        password: this.state.password
                    }
                )
            }).then(data => {
                console.log(data);
                const path = "/user/userlist";
                this.props.history.push(path);
            });
        }
    }

    toggleDanger() {
        this.setState({
          danger: !this.state.danger,
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Create User</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                            <Col>
                                <Form onSubmit={this.submitData} className="form-horizontal">
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Username</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="username" 
                                                    name="username"
                                                    value={this.state.username}
                                                    onChange={this.handleChange}
                                                    placeholder="Username" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Name</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="name" 
                                                    name="name"
                                                    value={this.state.name}
                                                    onChange={this.handleChange}
                                                    placeholder="Name" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Role</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="select" 
                                                    id="role" 
                                                    name="role"
                                                    value={this.state.role}
                                                    onChange={this.handleChange}
                                                    placeholder="Role">
                                                <option value="">Select Role</option>  
                                                <option value="Admin">Admin</option>  
                                                <option value="Operator">Operator</option>    
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Role ID</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="select" 
                                                    id="role_id" 
                                                    name="role_id"
                                                    value={this.state.role_id}
                                                    onChange={this.handleChange}
                                                    placeholder="Role ID">
                                                <option value="">Select Role</option>
                                                {
                                                    this.state.role_data.map((item) => {
                                                        return (
                                                            <option value={item.id}>{item.role_name}</option>
                                                        )
                                                    })
                                                }    
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Password</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="password" 
                                                    id="password" 
                                                    name="password"
                                                    value={this.state.password}
                                                    onChange={this.handleChange}
                                                    placeholder="Password" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Password</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="password" 
                                                    id="confirmPassword" 
                                                    name="confirmPassword"
                                                    value={this.state.confirmPassword}
                                                    onChange={this.handleChange}
                                                    placeholder="Confirm Password" />
                                        </Col>
                                    </FormGroup>
                                    <hr/>
                                    <FormGroup row>
                                        <Col align="center">
                                            <Button type="submit" size="lg" color="primary"><i className="fa fa-send-o"></i> Submit</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.danger} toggle={this.toggleDanger}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDanger}>Warning</ModalHeader>
                        <ModalBody>
                            Wrong Confirmation Password !
                        </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggleDanger}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default CreateUser;