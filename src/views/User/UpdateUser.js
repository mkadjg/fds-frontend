import React, {Component} from 'react';
import {
    Card,
    CardBody,
    Col,
    Row,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
} from 'reactstrap';

class UpdateUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: this.props.match.params.id ? this.props.match.params.id : '',
            username: '',
            name: '',
            role: '',
            role_id: '',
            role_data: [],
        }

        this.loadUserData = this.loadUserData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.submitUpdate = this.submitUpdate.bind(this);
        this.loadRoleData = this.loadRoleData.bind(this);
    }

    componentDidMount() {
        this.loadUserData();
        this.loadRoleData();
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    loadUserData() {
        fetch('http://localhost:5000/user/' + this.state.id, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(response => {
            this.setState({
                username: response.username,
                name: response.name,
                role: response.role,
                role_id: response.role_id
            })
        });
    }

    loadRoleData() {
        fetch('http://localhost:5000/roles/', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            this.setState({
                role_data: data
            })
        });
    }

    submitUpdate(event) {
        event.preventDefault();
        fetch('http://localhost:5000/user/' + this.state.id, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                username: this.state.username,
                name: this.state.name,
                role: this.state.role,
                role_id: this.state.role_id
            })
        }).then(response => {
            console.log(response)
            const path = "/user/userlist";
            this.props.history.push(path);
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Update User</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                            <Col>
                                <Form onSubmit={this.submitUpdate} className="form-horizontal">
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Username</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="username" 
                                                    name="username"
                                                    value={this.state.username}
                                                    onChange={this.handleChange}
                                                    placeholder="Username" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Name</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="name" 
                                                    name="name"
                                                    value={this.state.name}
                                                    onChange={this.handleChange}
                                                    placeholder="Name" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Role</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="select" 
                                                    id="role" 
                                                    name="role"
                                                    value={this.state.role}
                                                    onChange={this.handleChange}
                                                    placeholder="Role">
                                                <option value="">Select Role</option>  
                                                <option value="Admin">Admin</option>  
                                                <option value="Operator">Operator</option>    
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Role ID</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="select" 
                                                    id="role_id" 
                                                    name="role_id"
                                                    value={this.state.role_id}
                                                    onChange={this.handleChange}
                                                    placeholder="Role ID">
                                                <option value="">Select Role</option>
                                                {
                                                    this.state.role_data.map((item) => {
                                                        return (
                                                            <option value={item.id}>{item.role_name}</option>
                                                        )
                                                    })
                                                }    
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <hr/>
                                    <FormGroup row>
                                        <Col align="center">
                                            <Button type="submit" size="lg" color="primary"><i className="fa fa-send-o"></i> Update</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default UpdateUser;