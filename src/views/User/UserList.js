import React, {Component} from 'react';
import {
    Card,
    CardBody,
    Col,
    Row,
    Button,
    Table,
} from 'reactstrap';

class UserList extends Component{
    constructor(props){
        super(props);
        this.state = {
            tableData: []
        }

        this.loadUserList = this.loadUserList.bind(this);
    }

    componentDidMount() {
        this.loadUserList();
    }

    showCreateUser() {
        const path = "/user/createuser";
        this.props.history.push(path);
    }
        
    loadUserList() {
        fetch('http://localhost:5000/user/', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(response => {
            this.setState({
                tableData: response
            })
        });
    }

    deleteUser(id) {
        fetch('http://localhost:5000/user/' + id, {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            console.log(response);
            this.loadUserList();
        });
    }

    updateUser(id) {
        const path = "/user/updateuser/" + id;
        this.props.history.push(path);
    }

    render() {
        return(
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col xs="10">
                                        <h4>User List</h4>
                                        <hr></hr>
                                    </Col>
                                    <Col xs="2" align="center">
                                        <Button color="primary"
                                                onClick={
                                                    ()=>{
                                                        this.showCreateUser()
                                                    }
                                                }>
                                            <span className="fa fa-plus-square"></span> &nbsp; 
                                            Create New User
                                        </Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Username</th>
                                                    <th>Name</th>
                                                    <th>Role</th>
                                                    <th>Role ID</th>
                                                    <th>Role Name</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.tableData.map((item, index) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>{index + 1}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.username}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.name}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.role}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.role_id}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.role_name}</div>
                                                                </td>
                                                                <td>
                                                                    <Button color="success"
                                                                    onClick={()=> {this.updateUser(item.id)}}
                                                                    ><span className="fa fa-pencil"></span> Update
                                                                    </Button>
                                                                    &nbsp;
                                                                    <Button color="danger"
                                                                    onClick={()=> {this.deleteUser(item.id)}}
                                                                    ><span className="fa fa-trash"></span> Delete</Button>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default UserList;