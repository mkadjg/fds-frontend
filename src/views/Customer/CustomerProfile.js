import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    Form,
    FormGroup,
    Input,
    Label,
    Col,
    Row,
    Table,
    Badge,
} from 'reactstrap';


class CustomerProfile extends Component{
    constructor(props) {
        super(props);
        this.state = {
            norek: '',
            start_date: '',
            end_date: '',
            tableData : []
        }
        
        this.handleChange = this.handleChange.bind(this);
        this.loadTableData = this.loadTableData.bind(this);
        this.submitRequest = this.submitRequest.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    componentDidMount() {
        this.loadTableData();
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    loadTableData() {

        fetch('http://localhost:5000/costumer_profile/', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            this.setState({
                tableData : data
            })
        });
    }

    submitRequest(event) {
        event.preventDefault();
        fetch('http://localhost:5000/costumer_profile/', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(
                {
                    norek: this.state.norek,
                    start_date: this.state.start_date,
                    end_date: this.state.end_date
                }
            )
        }).then(function (response) {
            return response.json();
        }).then(data => {
            console.log("Berhasil " + data);
            this.handleReset();
            this.loadTableData();
        });
    }

    handleReset() {
        this.setState({
            norek: '',
            start_date: '',
            end_date: ''
        })
    }

    visualize(id) {
        const path = "/customer/visualize/" + id;
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Card>
                    <CardBody>
                        <Row>
                            <Col>
                                <h4>Filter</h4>
                                <hr/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form onSubmit={this.submitRequest} className="form-horizontal">
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Account Number</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="number" 
                                                    id="norek" 
                                                    name="norek"
                                                    value={this.state.norek}
                                                    onChange={this.handleChange}
                                                    placeholder="Account Number" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Time Period</Label>
                                        </Col>
                                        <Col xs="5">
                                            <Label>Start Date</Label>
                                            <Input  type="date" 
                                                    id="start_date" 
                                                    value={this.state.start_date}
                                                    onChange={this.handleChange}
                                                    name="start_date" />
                                        </Col>
                                        <Col xs="5">
                                            <Label>End Date</Label>
                                            <Input  type="date" 
                                                    id="end_date" 
                                                    value={this.state.end_date}
                                                    onChange={this.handleChange}
                                                    name="end_date" />
                                        </Col>
                                    </FormGroup>
                                    <hr/>
                                    <FormGroup row>
                                        <Col align="center">
                                            <Button type="submit" size="lg" color="primary"><i className="fa fa-send-o"></i> Request</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <Row>
                            <Col>
                                <h4>Historical Visualize Customer Transaction</h4>
                                <hr/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Visualize Date</th>
                                            <th>Account Number</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.tableData.map((item, index)=> {
                                                return (
                                                    <tr>
                                                        <td>
                                                            <div>{index + 1}</div>
                                                        </td>
                                                        <td>
                                                            <div>{item.request}</div>
                                                        </td>
                                                        <td>
                                                            <div>{item.norek}</div>
                                                        </td>
                                                        <td>
                                                            <div>{item.start_date}</div>
                                                        </td>
                                                        <td>
                                                            <div>{item.end_date}</div>
                                                        </td>
                                                        <td>
                                                            <div><Badge pill color="primary">{item.status}</Badge></div>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                <Button size="lg" color="primary"
                                                                        onClick={ ()=>{
                                                                            this.visualize(item.id)
                                                                        }}>
                                                                    <Label className="fa fa-bar-chart"></Label>
                                                                    Visualize
                                                                </Button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        )
    }
}

export default CustomerProfile;