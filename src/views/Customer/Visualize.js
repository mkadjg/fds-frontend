import React, {Component} from 'react';
import { Pie, Bar, Scatter } from 'react-chartjs-2';
import {
    Card,
    CardBody,
    Label,
    Col,
    Row,
    Button,
    Table,
} from 'reactstrap';
import CustomTooltips from '@coreui/coreui-plugin-chartjs-custom-tooltips';
var moment = require('moment')
var momentDurationFormatSetup = require("moment-duration-format")
momentDurationFormatSetup(moment)

const transactionFrequencyByNominalOption = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips
    },
    scales: {
        xAxes :[{ scaleLabel: { display: true, labelString: 'Amount' } }],
        yAxes: [{ scaleLabel: { display: true, labelString: 'Frequency' } }]
    },
    maintainAspectRatio: false
}

const transactionFrequencyByTimeOption = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips
    },
    scales: {
        xAxes :[{ scaleLabel: { display: true, labelString: 'Time' } }],
        yAxes: [{ scaleLabel: { display: true, labelString: 'Frequency' } }]
    },
    maintainAspectRatio: false
}

const tunaiTransactionClusteringByChannelOption = {
    scales: {
        xAxes :[
            { 
                scaleLabel: { display: true, labelString: 'Time' },
                ticks: {
                    userCallback: function(label) {
                        return moment.duration(label, "seconds").format("*hh:mm", { trunc: true });
                    }
                }  
            }
        ],
        
        yAxes: [
            { 
                scaleLabel: { display: true, labelString: 'Amount' },
                ticks: {
                    userCallback: function(label) {
                        return "Rp. "+label.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    }
                }
            }
        ]
    },
    tooltips: {
        callbacks: {
           label: function(tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || '';
              return label + ' (' + moment.duration(tooltipItem.xLabel, "seconds").format("*hh:mm", { trunc: true }) + ', ' + 'Rp. '+tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ') ';
           }
        }
    },
    maintainAspectRatio: false
}

const nonTunaiTransactionClusteringByChannelOption = {
    scales: {
        xAxes :[
            { 
                scaleLabel: { display: true, labelString: 'Time' },
                ticks: {
                    userCallback: function(label) {
                        return moment.duration(label, "seconds").format("*hh:mm", { trunc: true });
                    }
                }  
            }
        ],
        
        yAxes: [
            { 
                scaleLabel: { display: true, labelString: 'Amount' },
                ticks: {
                    userCallback: function(label) {
                        return "Rp. "+label.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    }
                }
            }
        ]
    },
    tooltips: {
        callbacks: {
           label: function(tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || '';
              return label + ' (' + moment.duration(tooltipItem.xLabel, "seconds").format("*hh:mm", { trunc: true }) + ', ' + 'Rp. '+tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ') ';
           }
        }
    },
    maintainAspectRatio: false
}

class Visualize extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id ? this.props.match.params.id : '',
            customerInformation: {
                accountNumber: '',
                cardNumber: '',
                startDate: '',
                endDate: '',
                customerName: '',
                customerNumber: ''
            },
            customerTransaction: [],
            topTunaiActivityByAmount: [],
            topTunaiActivityByFrequency: [],
            topNonTunaiActivityByAmount: [],
            topNonTunaiActivityByFrequency: [],
            transactionFrequencyByNominal: {},
            transactionFrequencyByTime: {},
            transactionAmountByChannel: {},
            transactionFrequencyByChannel: {},
            tunaiTransactionClusteringByChannel: {},
            nonTunaiTransactionClusteringByChannel: {},
            summaryOfTransactionByChannel: []
        }
        
        this.showQuery1 = this.showQuery1.bind(this);
        this.showQuery2 = this.showQuery2.bind(this);
        this.showQuery3 = this.showQuery3.bind(this);
        this.showQuery4 = this.showQuery4.bind(this);
        this.showQuery5 = this.showQuery5.bind(this);
        this.showQuery6 = this.showQuery6.bind(this);
        this.showQuery7 = this.showQuery7.bind(this);
        this.showQuery8 = this.showQuery8.bind(this);
        this.showQuery9 = this.showQuery9.bind(this);
        this.showQuery10 = this.showQuery10.bind(this);
        this.showQuery11 = this.showQuery11.bind(this);
        this.showQuery12 = this.showQuery12.bind(this);
    }

    componentDidMount() {
        this.fetchCustomerReport();
    }

    fetchCustomerReport() {
        fetch('http://localhost:5000/costumer_profile/' + this.state.id, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            var result = JSON.parse(data.result);
            this.showQuery1(result.query1);
            this.showQuery2(result.query2);
            this.showQuery3(result.query3);
            this.showQuery4(result.query4);
            this.showQuery5(result.query5);
            this.showQuery6(result.query6);
            this.showQuery7(result.query7);
            this.showQuery8(result.query8);
            this.showQuery9(result.query9);
            this.showQuery10(result.query10);
            this.showQuery11(result.query11);
            this.showQuery12(result.query12);
        });
    }

    showQuery1(data) {
        this.setState({
            customerTransaction: data
        })
    }

    showQuery2(data) {
        let newData = {
            labels: [],
            datasets: [
                {
                    label: 'Frequency',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: [],
                },
            ],
        };
        data.forEach(item => {
            newData.labels.push(item.kategori)
            newData.datasets[0].data.push(item.frequency) 
        });
        this.setState({
            transactionFrequencyByNominal : newData
        })
    }

    showQuery3(data) {
        let newData = {
            labels: [],
            datasets: [
                {
                label: 'Frequency',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: [],
                },
            ],
        };
        data.forEach(item => {
            newData.labels.push(item.time)
            newData.datasets[0].data.push(item.frequency) 
        });
        this.setState({
            transactionFrequencyByTime : newData
        })
    }

    showQuery4(data) {
        let newData = {
            labels: [],
            datasets: [
                {
                    label: 'INTERNET BANKING',
                    backgroundColor: '#38f5be',
                    borderColor: '#38f5be',
                    borderWidth: 1,
                    hoverBackgroundColor: '#38f5be',
                    hoverBorderColor: '#38f5be',
                    data: [],
                },
                {
                    label: 'SMS BANKING',
                    backgroundColor: '#997af8',
                    borderColor: '#997af8',
                    borderWidth: 1,
                    hoverBackgroundColor: '#997af8',
                    hoverBorderColor: '#997af8',
                    data: [],
                },
                {
                    label: 'EDC',
                    backgroundColor: '#293249',
                    borderColor: '#293249',
                    borderWidth: 1,
                    hoverBackgroundColor: '#293249',
                    hoverBorderColor: '#293249',
                    data: [],
                },
                {
                    label: 'ATM',
                    backgroundColor: '#f47f42',
                    borderColor: '#f47f42',
                    borderWidth: 1,
                    hoverBackgroundColor: '#f47f42',
                    hoverBorderColor: '#f47f42',
                    data: [],
                }
            ]
        };
        data.forEach(item => {
            if (item.tipe_channel === newData.datasets[0].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[0].data.push(index);
            } else if (item.tipe_channel === newData.datasets[1].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[1].data.push(index);
            } else if (item.tipe_channel === newData.datasets[2].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[2].data.push(index);
            } else if (item.tipe_channel === newData.datasets[3].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[3].data.push(index);
            }
        });

        this.setState({
            tunaiTransactionClusteringByChannel: newData
        });
    }

    showQuery5(data) {
        let newData = {
            labels: [],
            datasets: [
                {
                    label: 'INTERNET BANKING',
                    backgroundColor: '#38f5be',
                    borderColor: '#38f5be',
                    borderWidth: 1,
                    hoverBackgroundColor: '#38f5be',
                    hoverBorderColor: '#38f5be',
                    data: [],
                },
                {
                    label: 'SMS BANKING',
                    backgroundColor: '#997af8',
                    borderColor: '#997af8',
                    borderWidth: 1,
                    hoverBackgroundColor: '#997af8',
                    hoverBorderColor: '#997af8',
                    data: [],
                },
                {
                    label: 'EDC',
                    backgroundColor: '#293249',
                    borderColor: '#293249',
                    borderWidth: 1,
                    hoverBackgroundColor: '#293249',
                    hoverBorderColor: '#293249',
                    data: [],
                },
                {
                    label: 'ATM',
                    backgroundColor: '#f47f42',
                    borderColor: '#f47f42',
                    borderWidth: 1,
                    hoverBackgroundColor: '#f47f42',
                    hoverBorderColor: '#f47f42',
                    data: [],
                }
            ]
        };
        data.forEach(item => {
            if (item.tipe_channel === newData.datasets[0].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[0].data.push(index);
            } else if (item.tipe_channel === newData.datasets[1].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[1].data.push(index);
            } else if (item.tipe_channel === newData.datasets[2].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[2].data.push(index);
            } else if (item.tipe_channel === newData.datasets[3].label) {
                let index = {
                    x: item.time,
                    y: item.trx_amount
                }
                newData.datasets[3].data.push(index);
            }
        });

        this.setState({
            nonTunaiTransactionClusteringByChannel: newData
        });
    }

    showQuery6(data) {
        this.setState({
            topTunaiActivityByAmount: data
        });
    }

    showQuery7(data) {
        this.setState({
            topTunaiActivityByFrequency: data
        });
    }

    showQuery8(data) {
        this.setState({
            topNonTunaiActivityByAmount: data
        });
    }

    showQuery9(data) {
        this.setState({
            topNonTunaiActivityByFrequency: data
        });
    }

    showQuery10(data) {
        let newData = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                ],
            }],
        };
        data.forEach(item => {
            newData.labels.push(item.tipe_channel)
            newData.datasets[0].data.push(item.total) 
        });
        this.setState({
            transactionAmountByChannel : newData
        })
    }

    showQuery11(data) {
        let newData = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                ],
            }],
        };
        data.forEach(item => {
            newData.labels.push(item.tipe_channel)
            newData.datasets[0].data.push(item.frequency) 
        });
        this.setState({
            transactionFrequencyByChannel : newData
        })
    }

    showQuery12(data) {
        this.setState({
            summaryOfTransactionByChannel: data
        });
    }

    render() {
        return(
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Costumer Information</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="2">
                                        <Label>Account Number</Label>
                                    </Col>
                                    <Col xs="9">
                                        <Label>-</Label>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="2">
                                        <Label>Card Number</Label>
                                    </Col>
                                    <Col xs="9">
                                        <Label>-</Label>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="2">
                                        <Label>Time Period</Label>
                                    </Col>
                                    <Col xs="9">
                                        <Label>Time Period </Label> - <Label>Time Period</Label>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="2">
                                        <Label>Customer Name</Label>
                                    </Col>
                                    <Col xs="9">
                                        <Label>-</Label>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="2">
                                        <Label>Customer Number</Label>
                                    </Col>
                                    <Col xs="9">
                                        <Label>-</Label>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Costumer Transaction</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>Card Number</th>
                                                    <th>Amount</th>
                                                    <th>Channel</th>
                                                    <th>Transaction Time</th>
                                                    <th>Activity</th>
                                                    <th>Location</th>
                                                    <th>Status</th>
                                                    <th><Label className="fa fa-external-link"></Label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.customerTransaction.map((item)=>{
                                                    return (
                                                        <tr>
                                                            <td>
                                                                <div>{item.norek}</div>
                                                            </td>
                                                            <td>
                                                                <div>Rp. {item.trx_amount}</div>
                                                            </td>
                                                            <td>
                                                                <div>{item.tipe_channel}</div>
                                                            </td>
                                                            <td>
                                                                <div>{item.timestamp}</div>
                                                            </td>
                                                            <td>
                                                                <div>{item.jenis_trx}</div>
                                                            </td>
                                                            <td>
                                                                <div>No Information</div>
                                                            </td>
                                                            <td>
                                                                <div>{item.status_code === '00' ? 'Berhasil' : 'Gagal'}</div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <Button color="success">
                                                                        <Label className="fa fa-check"></Label>
                                                                    </Button>
                                                                    <Button color="success">
                                                                        <Label className="fa fa-check"></Label>
                                                                    </Button>
                                                                    <Button color="success">
                                                                        <Label className="fa fa-check"></Label>
                                                                    </Button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Transaction Frequency by Nominal</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <div className="chart-wrapper">
                                            <Bar    data={this.state.transactionFrequencyByNominal} 
                                                    options={transactionFrequencyByNominalOption} />
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Transaction Frequency by Time</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <div className="chart-wrapper">
                                            <Bar    data={this.state.transactionFrequencyByTime} 
                                                    options={transactionFrequencyByTimeOption} />
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Tunai Transaction Clustering by Channel</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <div className="chart-wrapper">
                                            <Scatter    data={this.state.tunaiTransactionClusteringByChannel} 
                                                        options={tunaiTransactionClusteringByChannelOption} />
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Non Tunai Transaction Clustering by Channel</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <div className="chart-wrapper">
                                            <Scatter    data={this.state.nonTunaiTransactionClusteringByChannel} 
                                                        options={nonTunaiTransactionClusteringByChannelOption} />
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col xs="6">
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Top Tunai Activity by Amount</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Activity</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.topTunaiActivityByAmount.map((item, index) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>{index + 1}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.jenis_trx}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.total}</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>

                    <Col xs="6">
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Top Tunai Activity by Frequency</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Activity</th>
                                                    <th>Frequency</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.topTunaiActivityByFrequency.map((item, index) =>{
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>{index + 1}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.jenis_trx}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.frequency}</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col xs="6">
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Top Non Tunai Activity by Amount</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Activity</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.topNonTunaiActivityByAmount.map((item, index) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>{index + 1}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.jenis_trx}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.total}</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>

                    <Col xs="6">
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Top Non Tunai Activity by Frequency</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Activity</th>
                                                    <th>Frequency</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.topNonTunaiActivityByFrequency.map((item, index) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>{index + 1}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.jenis_trx}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.frequency}</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col xs="6">
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Transaction Amount by Channel</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <div className="chart-wrapper">
                                            <Pie data={this.state.transactionAmountByChannel} />
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>

                    <Col xs="6">
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Transaction Frequency by Channel</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <div className="chart-wrapper">
                                            <Pie data={this.state.transactionFrequencyByChannel} />
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Summary of Transaction by Channel</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>Channel</th>
                                                    <th>Minimum Amount</th>
                                                    <th>Maximum Amount</th>
                                                    <th>Median Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.summaryOfTransactionByChannel.map((item) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>{item.tipe_channel}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.min}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.max}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.percentile_disc}</div>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Visualize;