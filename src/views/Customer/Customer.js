import React, {Component} from 'react';
import { Pie } from 'react-chartjs-2';
import {
    Card,
    CardBody,
    Col,
    Row,
    Button,
    ButtonGroup,
} from 'reactstrap';

class Customer extends Component {
    constructor(props){
        super(props);
        this.state = {
            transactionPie: {},
            amountPie: {}
        }

        this.loadInternetBankingFrequency = this.loadInternetBankingFrequency.bind(this);
        this.loadSmsBankingFrequency = this.loadSmsBankingFrequency.bind(this);
        this.loadEdcFrequency = this.loadEdcFrequency.bind(this);
        this.loadAtmFrequency = this.loadAtmFrequency.bind(this);
        this.loadAllFrequency = this.loadAllFrequency.bind(this);

        this.loadInternetBankingAmount = this.loadInternetBankingAmount.bind(this);
        this.loadSmsBankingAmount = this.loadSmsBankingAmount.bind(this);
        this.loadEdcAmount = this.loadEdcAmount.bind(this);
        this.loadAtmAmount = this.loadAtmAmount.bind(this);
        this.loadAllAmount = this.loadAllAmount.bind(this);
    }

    componentDidMount() {
        this.loadInternetBankingFrequency();
        this.loadInternetBankingAmount();
    }

    loadInternetBankingFrequency() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerInternetBankingByFrequency', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                transactionPie: pie
            })
        });
    }

    loadEdcFrequency() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerEdcByFrequency', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                transactionPie: pie
            })
        });
    }

    loadAtmFrequency() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerAtmByFrequency', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                transactionPie: pie
            })
        });
    }

    loadSmsBankingFrequency() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerSmsBankingByFrequency', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                transactionPie: pie
            })
        });
    }

    loadAllFrequency() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerAllByFrequency', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                transactionPie: pie
            })
        });
    }

    loadInternetBankingAmount() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerInternetBankingByAmount', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                amountPie: pie
            })
        });
    }

    loadSmsBankingAmount() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerSmsBankingByAmount', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                amountPie: pie
            })
        });
    }

    loadAtmAmount() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerAtmByAmount', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                amountPie: pie
            })
        });
    }

    loadEdcAmount() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerEdcByAmount', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                amountPie: pie
            })
        });
    }

    loadAllAmount() {

        var pie = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#23CA18',
                    '#C718CA'
                ],
            }],
        }

        fetch('http://localhost:5000/customer/top5CustomerAllByAmount', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            pie.labels = data.labels;
            pie.datasets[0].data = data.data;
            this.setState({
                amountPie: pie
            })
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Top 5 Customer</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="6">
                                        <Row>
                                            <Col align="center">
                                                <h4>Top 5 Transaction Frequency Customer By Channel Type</h4>
                                                <hr></hr>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <div className="chart-wrapper">
                                                    <Pie data={this.state.transactionPie} />
                                                </div>
                                            </Col>
                                        </Row>
                                        <br></br>
                                        <Row>
                                            <Col align="center">
                                                <ButtonGroup>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadInternetBankingFrequency()
                                                        }
                                                    }>INTERNET BANKING
                                                    </Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadSmsBankingFrequency()
                                                        }
                                                    }>SMS BANKING</Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadEdcFrequency()
                                                        }
                                                    }>EDC</Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadAtmFrequency()
                                                        }
                                                    }>ATM</Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadAllFrequency()
                                                        }
                                                    }>ALL</Button>
                                                </ButtonGroup>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs="6">
                                        <Row>
                                            <Col align="center">
                                                <h4>Top 5 Transaction Amount Customer By Channel Type</h4>
                                                <hr></hr>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <div className="chart-wrapper">
                                                    <Pie data={this.state.amountPie} />
                                                </div>
                                            </Col>
                                        </Row>
                                        <br></br>
                                        <Row>
                                            <Col align="center">
                                                <ButtonGroup>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadInternetBankingAmount()
                                                        }
                                                    }>INTERNET BANKING
                                                    </Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadSmsBankingAmount()
                                                        }
                                                    }>SMS BANKING</Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadEdcAmount()
                                                        }
                                                    }>EDC</Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadAtmAmount()
                                                        }
                                                    }>ATM</Button>
                                                    <Button onClick={
                                                        () => {
                                                            this.loadAllAmount()
                                                        }
                                                    }>ALL</Button>
                                                </ButtonGroup>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Customer;