import React, {Component} from 'react';
import {
    Card,
    CardBody,
    Col,
    Row,
    Button,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap';

class CreateRole extends Component {
    constructor(props){
        super(props);
        this.state = {
            role_name: '',
            description: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.submitData = this.submitData.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitData(event) {
        event.preventDefault();
        fetch('http://localhost:5000/roles/', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then(data => {
            console.log(data);
            const path = "/role/rolelist";
            this.props.history.push(path);
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Create Role</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                            <Col>
                                <Form onSubmit={this.submitData} className="form-horizontal">
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Role Name</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="role_name" 
                                                    name="role_name"
                                                    value={this.state.role_name}
                                                    onChange={this.handleChange}
                                                    placeholder="Role Name" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Description</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="description" 
                                                    name="description"
                                                    value={this.state.description}
                                                    onChange={this.handleChange}
                                                    placeholder="Description" />
                                        </Col>
                                    </FormGroup>
                                    <hr/>
                                    <FormGroup row>
                                        <Col align="center">
                                            <Button type="submit" size="lg" color="primary"><i className="fa fa-send-o"></i> Submit</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default CreateRole;