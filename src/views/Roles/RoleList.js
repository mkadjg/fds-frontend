import React, {Component} from 'react';
import {
    Card,
    CardBody,
    Col,
    Row,
    Button,
    Table,
} from 'reactstrap';

class RoleList extends Component {
    constructor(props){
        super(props);
        this.state = {
            tableData: []
        }

        this.loadRoleList = this.loadRoleList.bind(this);
    }

    componentDidMount() {
        this.loadRoleList();
    }

    loadRoleList() {
        fetch('http://localhost:5000/roles/', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(response => {
            this.setState({
                tableData: response
            })
        });
    }

    showCreateRole() {
        const path = "/role/createrole";
        this.props.history.push(path);
    }

    showUpdateRole(id) {
        const path = "/role/updaterole/" + id;
        this.props.history.push(path);
    }

    deleteRole(id) {
        fetch('http://localhost:5000/roles/' + id, {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            console.log(response);
            this.loadRoleList();
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col xs="10">
                                        <h4>Role List</h4>
                                        <hr></hr>
                                    </Col>
                                    <Col xs="2" align="center">
                                        <Button color="primary"
                                                onClick={
                                                    ()=>{
                                                        this.showCreateRole();
                                                    }
                                                }>
                                            <span className="fa fa-plus-square"></span> &nbsp; 
                                            Create New Role
                                        </Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Role Name</th>
                                                    <th>Description</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.tableData.map((item, index) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>{index + 1}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.role_name}</div>
                                                                </td>
                                                                <td>
                                                                    <div>{item.description}</div>
                                                                </td>
                                                                <td>
                                                                    <Button color="success"
                                                                    onClick={()=> {this.showUpdateRole(item.id)}}
                                                                    ><span className="fa fa-pencil"></span> Update</Button>
                                                                    &nbsp;
                                                                    <Button color="danger"
                                                                    onClick={()=> {this.deleteRole(item.id)}}
                                                                    ><span className="fa fa-trash"></span> Delete</Button>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default RoleList;