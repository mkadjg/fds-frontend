import React, {Component} from 'react';
import {
    Card,
    CardBody,
    Col,
    Row,
    Button,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap';

class UpdateRole extends Component {
    constructor(props){
        super(props);
        this.state = {
            id : this.props.match.params.id ? this.props.match.params.id : '',
            role_name: '',
            description: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.submitUpdate = this.submitUpdate.bind(this);
        this.loadRoleData = this.loadRoleData.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount() {
        this.loadRoleData();
    }

    loadRoleData() {
        fetch('http://localhost:5000/roles/' + this.state.id, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(response => {
            return response.json();
        }).then(data => {
            this.setState({
                role_name: data.role_name,
                description: data.description
            });
        });
    }

    submitUpdate(event) {
        event.preventDefault();
        fetch('http://localhost:5000/roles/' + this.state.id, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then(data => {
            console.log(data);
            const path = "/role/rolelist";
            this.props.history.push(path);
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <h4>Update Role</h4>
                                        <hr></hr>
                                    </Col>
                                </Row>
                                <Row>
                            <Col>
                                <Form onSubmit={this.submitUpdate} className="form-horizontal">
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Role Name</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="role_name" 
                                                    name="role_name"
                                                    value={this.state.role_name}
                                                    onChange={this.handleChange}
                                                    placeholder="Role Name" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="2">
                                            <Label>Description</Label>
                                        </Col>
                                        <Col xs="10">
                                            <Input  type="text" 
                                                    id="description" 
                                                    name="description"
                                                    value={this.state.description}
                                                    onChange={this.handleChange}
                                                    placeholder="Description" />
                                        </Col>
                                    </FormGroup>
                                    <hr/>
                                    <FormGroup row>
                                        <Col align="center">
                                            <Button type="submit" size="lg" color="primary"><i className="fa fa-send-o"></i> Update</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default UpdateRole;