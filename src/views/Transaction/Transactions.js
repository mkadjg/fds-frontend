import React, { Component } from 'react';
import { Line, Pie, Bar } from 'react-chartjs-2';
import CustomTooltips from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    CardColumns,
    Col,
    Row,
    Table,
} from 'reactstrap';

const options = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips
    },
    maintainAspectRatio: false
}

var pie = {
    labels: [],
    datasets: [{
        data: [],
        backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56',
        ],
        hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56',
        ],
    }],
}

var bar = {
    labels: [],
    datasets: [
      {
        label: 'Total Transaction',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: [],
      },
    ],
  };

var line = {
    labels: [],
    datasets: [
        {
            label: 'Total Transaction',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
        },
    ],
}

var month = [
    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
  ];

class Transactions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            line : {},
            bar: {},
            pie: {},
            tableData : []
        }

        this.loadTableData = this.loadTableData.bind(this);
        this.loadDataBarChart = this.loadDataBarChart.bind(this);
        this.loadDataTotalTransactionPerTypeCard = this.loadDataTotalTransactionPerTypeCard.bind(this);
        this.loadDataAllTransactionPerStatusCodeToday = this.loadDataAllTransactionPerStatusCodeToday.bind(this);
        this.loadDataTotalAmountTransactionPerStatusCodeToday = this.loadDataTotalAmountTransactionPerStatusCodeToday.bind(this);
        this.loadDataTotalTransactionLastHour = this.loadDataTotalTransactionLastHour.bind(this);
    }

    componentDidMount() {
        this.loadDataBarChart();
        this.loadDataTotalTransactionPerTypeCard();
        this.loadDataTotalTransactionLastHour();
        this.loadTableData();
        setInterval(() => {
            this.loadDataTotalTransactionLastHour();
            this.loadTableData();
        }, 30000);
    }

    componentWillUnmount() {
        clearInterval();
    }

    loadDataBarChart() {
        
        let barData = {};
        Object.assign(barData, bar);

        fetch('http://localhost:5000/transaction/getTotalTransactionByTipeChanelPerToday', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            barData.labels = data.labels;
            barData.datasets[0].data = data.data
            this.setState({
                bar : barData
            })
        });
    }

    loadDataTotalTransactionPerTypeCard() {
        
        let pieData = {};
        Object.assign(pieData, pie);

        fetch('http://localhost:5000/transaction/getCountTransactionByTypeCard', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            pieData.labels = data.labels;
            pieData.datasets[0].data = data.data
            this.setState({
                pie : pieData
            })
        });
    }

    loadDataAllTransactionPerStatusCodeToday() {

        let pieData = {};
        Object.assign(pieData, pie);

        fetch('http://localhost:5000/transaction/getTotalTransactionAllStatusToday', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            pieData.labels = data.labels;
            pieData.datasets[0].data = data.data
            this.setState({
                pie : pieData
            })
        });
    }

    loadDataTotalAmountTransactionPerStatusCodeToday() {

        let pieData = {};
        Object.assign(pieData, pie);

        fetch('http://localhost:5000/transaction/getTotalAmountAllStatusToday', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            pieData.labels = data.labels;
            pieData.datasets[0].data = data.data
            this.setState({
                pie : pieData
            })
        });
    }

    loadDataTotalTransactionLastHour() {
        let lineData = {};
        Object.assign(lineData, line);

        fetch('http://localhost:5000/getTotalTransactionLastHour', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            lineData.labels = data.labels;
            lineData.datasets[0].data = data.data
            this.setState({
                line : lineData
            })
        });
    }

    loadTableData() {

        fetch('http://localhost:5000/transaction/getLast20Transaction', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: null
        }).then(function (response) {
            return response.json();
        }).then(data => {
            this.setState({
                tableData : data
            })
        });
      } 

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col>
                        <CardColumns className="cols-2">
                            <Card>
                                <CardHeader>
                                    Transaksi Berhasil
                                    <div className="card-header-actions">
                                        <a href="http://www.chartjs.org" className="card-header-action">
                                        <small className="text-muted">docs</small>
                                        </a>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <div className="chart-wrapper" style={{ height: 353 + 'px', marginTop: 5 + 'px' }}>
                                        <Bar data={this.state.bar} options={options} />
                                    </div>
                                </CardBody>
                            </Card>

                            <Card>
                                <CardHeader>
                                    Transaksi Gagal
                                    <div className="card-header-actions">
                                        <Button color="primary" onClick={this.loadDataTotalTransactionPerTypeCard} >Jenis Kartu</Button>
                                        <Button color="primary" onClick={this.loadDataAllTransactionPerStatusCodeToday} >Status Transaksi</Button>
                                        <Button color="primary" onClick={this.loadDataTotalAmountTransactionPerStatusCodeToday} >Amount</Button>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <div className="chart-wrapper">
                                        <Pie data={this.state.pie} />
                                    </div>
                                </CardBody>
                            </Card>
                        </CardColumns>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                Live Transactions
                            </CardHeader>
                            <CardBody>
                                <div className="chart-wrapper" style={{ height: 353 + 'px', marginTop: 5 + 'px' }}>
                                    <Line data={this.state.line} options={options} />
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col lg="12">
                        <Card>
                            <CardHeader>
                                20 Transaksi Terakhir 
                            </CardHeader>
                            <CardBody>
                                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                    <thead className="thead-light">
                                        <tr>
                                        <th>No.Rekening</th>
                                        <th>Jenis Kartu</th>
                                        <th>Jenis Transaksi</th>
                                        <th>Tipe Channel</th>
                                        <th>Status Transaksi</th>
                                        <th>Waktu</th>
                                        <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.tableData.map((item) => {
                                            return (
                                                <tr>
                                                    <td>
                                                        <div>{item.norek}</div>
                                                    </td>
                                                    <td>
                                                        <div>{item.jenis_kartu}</div>
                                                    </td>
                                                    <td>
                                                        <div>{item.jenis_trx }</div>
                                                    </td>
                                                    <td>
                                                        <div>{item.tipe_channel}</div>
                                                    </td>
                                                    <td>
                                                        <div>{item.status_code === '00' ? 'Berhasil' : 'Gagal'}</div>
                                                    </td>
                                                    <td>
                                                        <div>{new Date(item.timestamp).getUTCDate() + ' ' + month[new Date(item.timestamp).getUTCMonth()] + ' ' + new Date(item.timestamp).getUTCFullYear() + ' ' + new Date(item.timestamp).getUTCHours() + ':' + new Date(item.timestamp).getUTCMinutes()}</div>
                                                    </td>
                                                    <td>
                                                        <div>{item.trx_amount}</div>
                                                    </td>
                                                </tr>
                                            );
                                        })}
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Transactions;