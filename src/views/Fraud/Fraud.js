import React, { Component } from 'react';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Form,
    FormGroup,
    Input,
    Label,
} from 'reactstrap';

class Fraud extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noRekening : '',
            jenisKartu : 0,
            tipeChannel : 0,
            nominal : 0,
            waktuKejadian : new Date().getFullYear() + '-' + (new Date().getMonth() + 1 < 10 ? '0' + (new Date().getMonth() + 1) :  (new Date().getMonth() + 1)) + '-' + new Date().getDate(),
            petugas : ''
        }

        this.handleNoRekening = this.handleNoRekening.bind(this);
        this.handleJenisKartu = this.handleJenisKartu.bind(this);
        this.handleTipeChannel = this.handleTipeChannel.bind(this);
        this.handleNominal = this.handleNominal.bind(this);
        this.handleWaktuKejadian = this.handleWaktuKejadian.bind(this);
        this.handlePetugas = this.handlePetugas.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    handleNoRekening(event) {
        this.setState({
            noRekening : event.target.value
        })
    }

    handleJenisKartu(event) {
        this.setState({
            jenisKartu : event.target.value
        })
    }

    handleTipeChannel(event) {
        this.setState({
            tipeChannel : event.target.value
        })
    }

    handleNominal(event) {
        this.setState({
            nominal : event.target.value
        })
    }

    handleWaktuKejadian(event) {
        this.setState({
            waktuKejadian : event.target.value
        })
    }

    handlePetugas(event) {
        this.setState({
            petugas : event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        fetch('http://localhost:5000/insertDataFraudReport', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then(data => {
            console.log(data);
        });
    }

    handleReset(event) {
        event.preventDefault();
        this.setState({
            noRekening : '',
            jenisKartu : 0,
            tipeChannel : 0,
            nominal : 0,
            waktuKejadian : Date.now(),
            petugas : ''
        })
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col md="6">
                        <Card>
                            <CardHeader>
                                Fraud Report
                            </CardHeader>
                            <CardBody>
                                <Form action="" onSubmit={this.handleSubmit} onReset={this.handleReset} className="form-horizontal">
                                    <FormGroup row>
                                        <Col xs="12" md="3">
                                            <Label>No.Rekening</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Input  type="number" 
                                                    id="noRekening" 
                                                    name="noRekening"
                                                    value={this.state.noRekening}
                                                    onChange={this.handleNoRekening} 
                                                    placeholder="Masukan Nomor Rekening" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="12" md="3">
                                            <Label>Jenis Kartu</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Input  type="select" 
                                                    name="jenisKartu" 
                                                    id="jenisKartu"
                                                    value={this.state.jenisKartu}
                                                    onChange={this.handleJenisKartu}>
                                                <option value="0">Pilih Jenis Kartu</option>
                                                <option value="Britama">Britama</option>
                                                <option value="Priorotas">Prioritas</option>
                                                <option value="Simpedes">Simpedes</option>
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="12" md="3">
                                            <Label>Tipe Channel</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Input  type="select" 
                                                    name="tipeChannel" 
                                                    id="tipeChannel"
                                                    value={this.state.tipeChannel}
                                                    onChange={this.handleTipeChannel}>
                                                <option value="0">Pilih Tipe Channel</option>
                                                <option value="M-Banking">M-Banking</option>
                                                <option value="ATM Transfer">ATM Transfer</option>
                                                <option value="SMS-Banking">SMS-Banking</option>
                                                <option value="Internet-Banking">Internet-Banking</option>
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="12" md="3">
                                            <Label>Nominal</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Input  type="number" 
                                                    id="nominal" 
                                                    name="nominal"
                                                    value={this.state.nominal}
                                                    onChange={this.handleNominal} 
                                                    placeholder="Masukan Jumlah Nominal" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="12" md="3">
                                            <Label htmlFor="waktuKejadian">Waktu Kejadian</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Input  type="date" 
                                                    id="waktuKejadian" 
                                                    name="waktuKejadian"
                                                    value={this.state.waktuKejadian}
                                                    onChange={this.handleWaktuKejadian} 
                                                    placeholder="date" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col xs="12" md="3">
                                            <Label>Petugas</Label>
                                        </Col>
                                        <Col xs="12" md="9">
                                            <Input  type="text" 
                                                    id="petugas" 
                                                    name="petugas"
                                                    value={this.state.petugas}
                                                    onChange={this.handlePetugas} 
                                                    placeholder="Masukan Nama Petugas" />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup>
                                        <Button type="submit" size="sm" color="success"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                                        <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Fraud;